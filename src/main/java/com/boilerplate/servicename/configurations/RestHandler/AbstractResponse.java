package com.boilerplate.servicename.configurations.RestHandler;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;

import java.util.Date;

public class AbstractResponse implements Response {

    private Date timestamp;

    private Integer status;

    private Object data;

    @JsonIgnore
    private String message;

    public AbstractResponse() {
        this.timestamp = new Date();
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    protected AbstractResponse response(HttpStatus status, Object payload) {
        setStatus(status.value());
        setData(payload);
        return this;
    }
}
