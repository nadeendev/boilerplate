package com.boilerplate.servicename.configurations.RestHandler;

import java.util.Date;

public interface Response {

    Date getTimestamp();

    void setTimestamp(Date timestamp);

    Integer getStatus();

    void setStatus(Integer status);

    Object getData();

    void setData(Object data);

    String getMessage();

    void setMessage(String message);

}
