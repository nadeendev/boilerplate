package com.boilerplate.servicename.controllers;

import com.boilerplate.servicename.configurations.RestHandler.AbstractResponse;
import com.boilerplate.servicename.configurations.RestHandler.Response;
import com.boilerplate.servicename.exceptions.ResourceNotFoundException;
import com.boilerplate.servicename.models.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ping")
public class PingController extends AbstractResponse {

    @GetMapping
    public Response ping() {
        return response(HttpStatus.OK, new Employee("Nadeen", "Gamage"));
    }

    @GetMapping("/exception")
    public void exception() {
        throw new ResourceNotFoundException("Resource Not Found!");
    }
}
